//! Pipes and fixtures, not generally useful.
//!
//! The functionality in this module is only useful for crates
//! implementing the [`SOP`] interface, and very specialized consumers
//! of it.  It is likely that you don't need it.

use crate::{
    SOP,
};

/// Returns a reference to SOP.
pub trait SopRef<'s, S: SOP<'s>> {
    /// Returns a reference to SOP.
    fn sop(&self) -> &'s S;
}

pub trait PasswordsAreHumanReadable {
    /// Returns the normalized password.
    ///
    /// Use this function when you generate an artifact using a
    /// password (see [Generating Material with Human-Readable
    /// Passwords]).
    ///
    /// [Generating Material with Human-Readable Passwords]: https://www.ietf.org/archive/id/draft-dkg-openpgp-stateless-cli-08.html#name-generating-material-with-hu
    fn normalized(&self) -> &[u8];

    /// Returns the password in all possible variants.
    ///
    /// Use this function to try all possible variants
    /// (i.e. normalized, as-is, ..) when consuming an artifact (see
    /// [Consuming Password-protected Material]).
    ///
    /// [Consuming Password-protected Material]: https://www.ietf.org/archive/id/draft-dkg-openpgp-stateless-cli-08.html#name-consuming-password-protecte
    fn variants(&self) -> Box<dyn Iterator<Item = &[u8]> + '_>;
}
